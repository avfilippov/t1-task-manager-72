package ru.t1.avfilippov.tm.enumerated;

public enum RoleType {

    USER, ADMINISTRATOR;

}
