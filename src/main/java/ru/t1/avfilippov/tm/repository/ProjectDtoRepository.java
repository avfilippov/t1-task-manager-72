package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;

import java.util.List;
import java.util.Optional;

public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    long countByUserId(String userId);

    Optional<ProjectDto> findByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

    List<ProjectDto> findAllByUserId(String userId);

}
