package ru.t1.avfilippov.tm.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public class UserDto {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String login;

    @Column
    private String passwordHash;

}
