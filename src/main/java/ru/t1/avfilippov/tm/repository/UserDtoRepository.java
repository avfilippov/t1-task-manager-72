package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.dto.UserDto;


public interface UserDtoRepository extends JpaRepository<UserDto, String> {

    UserDto findByLogin(String login);

}
