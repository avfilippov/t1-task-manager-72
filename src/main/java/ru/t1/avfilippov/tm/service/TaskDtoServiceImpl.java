package ru.t1.avfilippov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.repository.TaskDtoRepository;
import ru.t1.avfilippov.tm.util.UserUtil;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskDtoServiceImpl implements TaskDTOService {

    @Autowired
    private TaskDtoRepository taskRepository;

    @Override
    public List<TaskDto> findAll(final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public TaskDto save(final TaskDto task) {
        return taskRepository.save(task);
    }

    @Override
    public TaskDto save(final String userId) {
        final TaskDto task = new TaskDto("New Task " + LocalDateTime.now().toString());
        task.setUserId(UserUtil.getUserId());
        return taskRepository.save(task);
    }

    @Override
    public TaskDto findById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existsById(final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    public TaskDto findByUserIdAndId(final String userId, final String id) {
        return taskRepository.findByUserIdAndId(userId, id).orElse(null);
    }

    @Override
    public boolean existsByUserIdAndId(final String userId, final String id) {
        return taskRepository.findByUserIdAndId(userId, id).isPresent();
    }

    @Override
    public void deleteByUserIdAndId(final String userId, final String id) {
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public long countByUserId(final String userId) {
        return taskRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteById(final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(final TaskDto task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void deleteAll(final List<TaskDto> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    @Transactional
    public void clear(final String userId) {
        taskRepository.deleteByUserId(userId);
    }

}
