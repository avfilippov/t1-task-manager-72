package ru.t1.avfilippov.tm.client;

import ru.t1.avfilippov.tm.api.endpoint.ProjectEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectSoapEndpointClient {

    public static ProjectEndpoint getInstance(final String baseURL) throws MalformedURLException {
        final String wsdl = baseURL + "/ws/ProjectEndpoint?wsdl";
        final URL url = new URL(wsdl);
        final String lp = "ProjectDtoServiceImpl";
        final String ns = "http://endpoint.tm.avfilippov.t1.ru/";
        final QName name = new QName(ns, lp);
        final ProjectEndpoint result = Service.create(url, name).getPort(ProjectEndpoint.class);
        final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
